﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using KitchenSinkBDD.DataFactory.BlogData;

namespace KitchenSinkBDD.DataFactory
{
    public class NinjectBinder : NinjectModule
    {
        public override void Load()
        {
            //Kernel.Load(new KitchenSinkBDD.Repositories.NinjectBinder());
            //Commands
            Kernel.Bind<IBlogDataFactory>().To<BlogDataFactory>().InRequestScope(); // verified. singleton
        }
        
    }
}
