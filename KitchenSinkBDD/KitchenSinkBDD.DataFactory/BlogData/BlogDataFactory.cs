﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.DataFactory.BlogData
{
    public class BlogDataFactory : IBlogDataFactory
    {
        private KitchenSinkBDD.Repositories.Blogs.IBlogRepository _repository;
        public BlogDataFactory(KitchenSinkBDD.Repositories.Blogs.IBlogRepository repository)
        {
            this._repository = repository;
        }

        public IEnumerable<KitchenSinkBDD.Models.Blogs.Blog> GetAll()
        {
            return _repository.GetAll();
        }

        public KitchenSinkBDD.Models.Blogs.Blog Get(int id)
        {
            return _repository.Get(id);
        }

        public KitchenSinkBDD.Models.Blogs.Post GetPost(int id)
        {
            //IEnumerable<KitchenSinkBDD.Models.Blogs.Blog> blogs = this.GetAll();
            //IEnumerable<KitchenSinkBDD.Models.Blogs.Post> posts = blogs.Where(b=>b.Posts != null).SelectMany(b=>b.Posts);
            //KitchenSinkBDD.Models.Blogs.Post post = posts.Where(p=>p.PostId == id).FirstOrDefault();
            /*KitchenSinkBDD.Models.Blogs.Post post = (from b in blogs
                                                     from p in b.Posts
                                                     where p.PostId == id
                                                     select p).FirstOrDefault();*/
            //return post;
            return _repository.GetPost(id);
        }
    }
}
