﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.DataFactory.BlogData
{
    public interface IBlogDataFactory
    {
        IEnumerable<KitchenSinkBDD.Models.Blogs.Blog> GetAll();
        KitchenSinkBDD.Models.Blogs.Blog Get(int id);

        KitchenSinkBDD.Models.Blogs.Post GetPost(int id);
    }
}
