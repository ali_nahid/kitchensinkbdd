﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using Ninject.Modules;
using Ninject.Extensions.Conventions;
using Ninject.Web.Common;
using KitchenSinkBDD.Repositories.Blogs;

namespace KitchenSinkBDD.Processor
{
    public class NinjectBinder : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new KitchenSinkBDD.Exceptions.NinjectBinder());

            Kernel.Load(new KitchenSinkBDD.Repositories.NinjectBinder());

            //Commands
            Kernel.Bind<ICommandBus>().To<CommandBus>().InSingletonScope(); // verified. singleton

            // bind command handlers and event handlers. these need to be bound together as classes which implement both handles<> and subscribes<> were being bound twice leading to errors.
            Kernel.Bind(x =>
            {
                x.FromThisAssembly().SelectAllClasses().InheritedFromAny(typeof(IHandles<>)).BindAllInterfaces().Configure(c => c.InTransientScope());
            });
            
        }
        
    }
}
