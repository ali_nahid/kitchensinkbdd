﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KitchenSinkBDD.Processor.BlogProcessor.Commands;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using KitchenSinkBDD.Repositories.Blogs;
using KitchenSinkBDD.Exceptions.Blog;

namespace KitchenSinkBDD.Processor.BlogProcessor
{
    public class DeleteBlogCommandHandler : IHandles<UpdateBlogCommand>
    {
        private ExceptionManager exManager;
        private IBlogRepository repository;
        public DeleteBlogCommandHandler(ExceptionManager eManager, IBlogRepository repo)
        {
            this.exManager = eManager;
            this.repository = repo;
        }

        public void Handle(UpdateBlogCommand command)
        {
            exManager.Process(() =>
            {
                throw new NotImplementedException(string.Format("Delete not available for this command {0}", this));
            }, KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.ProcessorPolicy);
        }
    }
}
