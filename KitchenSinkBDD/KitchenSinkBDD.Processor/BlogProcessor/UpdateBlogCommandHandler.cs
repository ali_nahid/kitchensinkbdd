﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KitchenSinkBDD.Processor.BlogProcessor.Commands;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using KitchenSinkBDD.Repositories.Blogs;
using KitchenSinkBDD.Exceptions.Blog;

namespace KitchenSinkBDD.Processor.BlogProcessor
{
    public class UpdateBlogCommandHandler : IHandles<UpdateBlogCommand>
    {
        private ExceptionManager exManager;
        private IBlogRepository repository;
        public UpdateBlogCommandHandler(ExceptionManager eManager, IBlogRepository repo)
        {
            this.exManager = eManager;
            this.repository = repo;
        }

        public void Handle(UpdateBlogCommand command)
        {
            exManager.Process(() =>
            {
                if(!repository.Update(command.BlogId, command.BlogName))
                {
                    throw new BlogError();
                }
            }, KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.ProcessorPolicy);
        }
    }
}
