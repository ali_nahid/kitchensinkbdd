﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KitchenSinkBDD.Processor.BlogProcessor.Commands;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using KitchenSinkBDD.Repositories.Blogs;
using KitchenSinkBDD.Exceptions.Blog;

namespace KitchenSinkBDD.Processor.BlogProcessor
{
    public class CreateBlogCommandHandler : IHandles<CreateBlogCommand>
    {
        private ExceptionManager exManager;
        private IBlogRepository repository;
        public CreateBlogCommandHandler(ExceptionManager eManager, IBlogRepository repo)
        {
            this.exManager = eManager;
            this.repository = repo;
        }

        public void Handle(CreateBlogCommand command)
        {
            exManager.Process(() =>
            {
                if(!repository.Create(command.BlogName))
                {
                    throw new BlogError(string.Format("blog with name {0} already exists in the system.", command.BlogName));
                }
                
            }, KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.ProcessorPolicy);
        }
    }
}
