﻿using KitchenSinkBDD.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Processor.BlogProcessor.Commands
{
    public class CreateOrUpdatePostCommand : BaseCommand, ICommand
    {
        public string Title { get; set; }
        public string Content { get;set; }
        public string BlogName {get;set;}

        public CommandOperation Operation { get; set; }

        public CreateOrUpdatePostCommand(string blog, string title, string content, CommandOperation operation)
        {
            this.BlogName = blog;
            this.Title = title;
            this.Content = content;
            this.Operation = operation;
        }
    }
}
