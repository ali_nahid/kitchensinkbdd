﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Processor.BlogProcessor.Commands
{
    public class UpdateBlogCommand : BaseCommand, ICommand
    {
        public string BlogName { get; set; }
        public int BlogId { get; set; }
        public UpdateBlogCommand(int id, string name)
        {
            this.BlogId = id;
            this.BlogName = name;
        }
    }
}
