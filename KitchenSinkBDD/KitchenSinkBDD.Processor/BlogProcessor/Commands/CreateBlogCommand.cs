﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Processor.BlogProcessor.Commands
{
    public class CreateBlogCommand : BaseCommand, ICommand
    {
        public string BlogName { get; set; }
        public CreateBlogCommand(string name)
        {
            this.BlogName = name;
        }
    }
}
