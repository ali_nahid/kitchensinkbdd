﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KitchenSinkBDD.Processor.BlogProcessor.Commands;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using KitchenSinkBDD.Repositories.Blogs;
using KitchenSinkBDD.Exceptions.Blog;

namespace KitchenSinkBDD.Processor.BlogProcessor
{
    public class CreateOrUpdatePostCommandHandler : IHandles<CreateOrUpdatePostCommand>
    {
        private ExceptionManager exManager;
        private IBlogRepository repository;
        public CreateOrUpdatePostCommandHandler(ExceptionManager eManager, IBlogRepository repo)
        {
            this.exManager = eManager;
            this.repository = repo;
        }

        public void Handle(CreateOrUpdatePostCommand command)
        {
            exManager.Process(() =>
            {        
                KitchenSinkBDD.Models.Blogs.Blog blog = repository.Get(command.BlogName);
                if(blog == null)
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("blog with name {0} not found. New post cannot be added.", command.BlogName));
                }
                if(command.Operation == Constants.CommandOperation.Create)
                {
                    repository.AddNewPost(blog, command.Title, command.Content);
                }
                else if(command.Operation == Constants.CommandOperation.Update)
                {
                    repository.UpdatePost(blog, command.Title, command.Content);
                }
                else if(command.Operation == Constants.CommandOperation.Delete)
                {
                    throw new NotImplementedException();
                }
                
            }, KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.ProcessorPolicy);
        }
    }
}
