﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Processor
{
    public interface IHandles<T> where T : class, ICommand
    {
        void Handle(T command);
    } 
}
