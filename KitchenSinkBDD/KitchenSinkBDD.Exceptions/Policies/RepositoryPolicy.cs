﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using KitchenSinkBDD.Exceptions.Errors;
using System.Diagnostics;

namespace KitchenSinkBDD.Exceptions.Policies
{
    public class RepositoryPolicy : IExceptionPolicy<RepositoryPolicy>
    {
        public List<ExceptionPolicyEntry> GetPolicyEntry(LogWriter logWriter)
        {
            List<ExceptionPolicyEntry> repositoryPolicy = new List<ExceptionPolicyEntry>()
            {
                new ExceptionPolicyEntry(typeof (ErrorBase),
                    PostHandlingAction.NotifyRethrow,
                    new IExceptionHandler[]
                     {
                       new LoggingExceptionHandler("General", 9001, TraceEventType.Error,
                         "Repository Exception", 5, typeof(TextExceptionFormatter), logWriter)
                     }),
                new ExceptionPolicyEntry(typeof (Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                     {
                       new LoggingExceptionHandler("General", 9001, TraceEventType.Error,
                         "Unhandled Repository Exception", 5, typeof(TextExceptionFormatter), logWriter),
                       new WrapHandler("System Error. Please contact your administrator.",
                         typeof(RepositoryError))
                     })
            };
            return repositoryPolicy;
        }
    }
}
