﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Exceptions.Errors
{
    public abstract class ErrorBase : System.SystemException
    {
        public String ErrorCode { get; set; }
        public String ErrorDescription { get; set; }
        public String ErrorType { get; set; }

        public ErrorBase()
        {
            this.ErrorType = this.GetType().ToString();
        }

        public ErrorBase(String message): base(message)
        {
            this.ErrorType = this.GetType().ToString();
            this.ErrorDescription = message;
        }

        public ErrorBase(string message, Exception innerException) : base(message, innerException)
        {
            this.ErrorType = this.GetType().ToString();
            this.ErrorDescription = message;
        }
    }
}
