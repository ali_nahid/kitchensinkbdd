﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Exceptions.Errors
{
    public class ProcessorError : ErrorBase
    {
        public ProcessorError()
        {

        }

        public ProcessorError(String message)
            : base(message)
        {

        }

        public ProcessorError(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
