﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Exceptions.Errors
{
    public class RepositoryError : ErrorBase
    {
        public RepositoryError()
        {

        }

        public RepositoryError(String message)
            : base(message)
        {

        }

        public RepositoryError(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
