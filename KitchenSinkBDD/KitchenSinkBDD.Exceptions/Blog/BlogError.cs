﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KitchenSinkBDD.Exceptions.Errors;

namespace KitchenSinkBDD.Exceptions.Blog
{
    public class BlogError : RepositoryError
    {
        public BlogError()
        {

        }

        public BlogError(String message)
            : base(message)
        {

        }

        public BlogError(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
