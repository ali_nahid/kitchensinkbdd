﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Exceptions
{
    public interface IExceptionManagerProfiles
    {
        ExceptionManager BuildExceptionManagerConfig(LogWriter logWriter);
    }
}
