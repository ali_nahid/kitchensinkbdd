﻿using KitchenSinkBDD.Exceptions.Policies;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Exceptions
{
    public class ExceptionManagerProfiles : IExceptionManagerProfiles
    {
        private IExceptionPolicy<RepositoryPolicy> repositoryPolicy;
        private IExceptionPolicy<ProcessorPolicy> processorPolicy;
        public ExceptionManagerProfiles(IExceptionPolicy<RepositoryPolicy> repoPolicy, IExceptionPolicy<ProcessorPolicy> processorPolicy)
        {
            this.repositoryPolicy = repoPolicy;
            this.processorPolicy = processorPolicy;
        }
        public ExceptionManager BuildExceptionManagerConfig(LogWriter logWriter)
        {

            var policies = new List<ExceptionPolicyDefinition>();
            var repositoryPolicyEntries = this.repositoryPolicy.GetPolicyEntry(logWriter);
            policies.Add(new ExceptionPolicyDefinition("RepositoryPolicy", repositoryPolicyEntries));

            var processorPolicyEntries = this.processorPolicy.GetPolicyEntry(logWriter);
            policies.Add(new ExceptionPolicyDefinition("ProcessorPolicy", processorPolicyEntries));

            return new ExceptionManager(policies);
        }
    }
}
