﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Ninject;
using Ninject.Modules;
using Ninject.Extensions.Conventions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

using KitchenSinkBDD.Exceptions.Policies;
using KitchenSinkBDD.Exceptions.Logging;

namespace KitchenSinkBDD.Exceptions
{
    public class NinjectBinder : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x =>
            {
                x.FromThisAssembly().SelectAllClasses().InheritedFromAny(typeof(IExceptionPolicy<>)).BindAllInterfaces().Configure(c => c.InTransientScope());
            });

            //Configurators          
            Kernel.Bind<IExceptionManagerProfiles>().To<ExceptionManagerProfiles>().InSingletonScope(); // verified. singleton
            Kernel.Bind<ILoggingConfigurator>().To<LoggingConfigurator>().InSingletonScope(); // verified. singleton

            Kernel.Bind<ExceptionManager>().ToProvider<ExceptionManagerFactory>().InSingletonScope(); // verified. singleton.
        }
    }
}
