﻿Feature: BlogService
	In order to avoid silly mistakes
	As a author
	I want to be assured that post service is working ok

@TestBlogServiceSteps
Scenario: Service.Create New Post When Blog Doesnt exist
	Given If I try to add the following post
	| BlogName              | Title | Content |
	| Non Existing Food Blog | Food Post 1 | this is test food post 1 content |
	Then the system should "fail".

Scenario: Service.Create New Post When Blog Exists
	Given If I try to add the following post
	| BlogName              | Title | Content |
	| Food Blog | Food Post 1 | this is test food post 1 content |
	Then the system should "pass".


Scenario: Service.Create New Blog that exists
	Given If I try to create the following blog
	| BlogName              |
	| Java Blog |
	Then the system should "fail".

Scenario: Service.Create New Blog that doesnt exist
	Given If I try to create the following blog
	| BlogName              |
	| BDD is awesome Blog |
	Then the system should "pass".
