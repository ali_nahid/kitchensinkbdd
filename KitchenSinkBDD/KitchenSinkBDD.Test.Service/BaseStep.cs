﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace KitchenSinkBDD.Test.Service
{
    public abstract class BaseStep
    {
        protected void AddToScenario<T>(string key, T value)
        {
            if (ScenarioContext.Current.ContainsKey(key))
            {
                ScenarioContext.Current.Set<T>(value, key);
            }
            else
            {
                ScenarioContext.Current.Add(key, value);
            }

        }

        protected T GetFromScenario<T>(string key)
        {
            if (ScenarioContext.Current.ContainsKey(key))
            {
                return ScenarioContext.Current.Get<T>(key);
            }
            object obj = null;
            return (T)obj;
        }
    }
}
