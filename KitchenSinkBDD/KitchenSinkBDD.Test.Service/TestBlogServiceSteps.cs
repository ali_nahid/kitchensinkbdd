﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

using Ninject;
using Ninject.Web.Common;
using KitchenSinkBDD.Services.BloggingServices;
using KitchenSinkBDD.Services.BloggingServices.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KitchenSinkBDD.Test.Service
{
    [Binding]
    public class TestBlogServiceSteps : BaseStep
    {
        private IBlogService service;
        public TestBlogServiceSteps()
        {
            IKernel kernel = new StandardKernel(new KitchenSinkBDD.Services.NinjectBinder());
            service = kernel.Get<IBlogService>();
        }

        [Given(@"If I try to add the following post")]
        public void IfITryToAddPost(Table table)
        {
            foreach (TableRow row in table.Rows)
            {
                string blogName = row["BlogName"];
                string title = row["Title"];
                string content = row["Content"];
                try
                {
                    service.DoNewPost(blogName, title, content);
                    this.AddToScenario<string>("PassOrFail", "pass");
                }
                catch(Exception ex)
                {
                    this.AddToScenario<string>("PassOrFail", "fail");
                    this.AddToScenario<string>("ErrorMessage", ex.Message);
                }
            }
            
        }

        [Given(@"If I try to create the following blog")]
        public void IfITryToCreateBlog(Table table)
        {
            foreach (TableRow row in table.Rows)
            {
                string blogName = row["BlogName"];
                try
                {
                    service.CreateBlog(blogName);
                    this.AddToScenario<string>("PassOrFail", "pass");
                }
                catch (Exception ex)
                {
                    this.AddToScenario<string>("PassOrFail", "fail");
                    this.AddToScenario<string>("ErrorMessage", ex.Message);
                }
            }
        }

        [Given(@"If I try to Update the following blog")]
        public void IfITryToUpdateBlog(Table table)
        {
            foreach (TableRow row in table.Rows)
            {
                string blogName = row["BlogName"];
                try
                {
                    service.CreateBlog(blogName);
                    this.AddToScenario<string>("PassOrFail", "pass");
                }
                catch (Exception ex)
                {
                    this.AddToScenario<string>("PassOrFail", "fail");
                    this.AddToScenario<string>("ErrorMessage", ex.Message);
                }
            }
        }

        [Then(@"the system should ""(.*)"".")]
        public void TheSystemShould(string resultShouldBe)
        {
            System.Diagnostics.Trace.WriteLine(this.GetFromScenario<string>("ErrorMessage"));
            Assert.AreEqual(resultShouldBe, this.GetFromScenario<string>("PassOrFail"), true, this.GetFromScenario<string>("ErrorMessage"));
        }

    }
}
