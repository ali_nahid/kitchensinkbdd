﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using KitchenSinkBDD.Repositories.Blogs;

namespace KitchenSinkBDD.Repositories
{
    public class NinjectBinder : NinjectModule
    {
        public override void Load()
        {
            if (Kernel.GetBindings(typeof(Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionManager)).Count() < 1)
            {
                Kernel.Load(new KitchenSinkBDD.Exceptions.NinjectBinder());
            }
            //Db Context
            Kernel.Bind<EF.BDDContext>().ToSelf().InRequestScope();
            //Repostiories - all in request scope
            Kernel.Bind<IBlogRepository>().To<BlogRepository>().InRequestScope();

        }
        
    }
}
