﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Repositories
{
    public abstract class BaseRepository<T> where T : KitchenSinkBDD.Models.Aggregate
    {
        protected KitchenSinkBDD.EF.BDDContext _dbContext = null;
        public BaseRepository(KitchenSinkBDD.EF.BDDContext dbContext)
        {
            _dbContext = dbContext;
        }


        public virtual int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }
    }
}
