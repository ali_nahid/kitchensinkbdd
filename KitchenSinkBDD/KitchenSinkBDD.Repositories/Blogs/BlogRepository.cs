﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using KitchenSinkBDD.Models.Blogs;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace KitchenSinkBDD.Repositories.Blogs
{
    public class BlogRepository : BaseRepository<Blog>, IBlogRepository
    {
        private ExceptionManager exManager;
        public BlogRepository(ExceptionManager eManager, KitchenSinkBDD.EF.BDDContext dbContext) : base (dbContext)
        {
            this.exManager = eManager;
        }

        public bool Create(string name)
        {
            bool result = false;
            exManager.Process(()=>
                {
                    result = this.CreateBlogProcess(name);
                },
                KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.RepositoryPolicy
            );
            return result;
        }

        public bool Update(int id, string name)
        {
            bool result = false;
            exManager.Process(() =>
            {
                result = this.UpdateBlogProcess(id,name);
            },
                KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.RepositoryPolicy
            );
            return result;
        }

        public void Delete(int id)
        {
            exManager.Process(() =>
            {
                this.DeleteBlogProcess(id);
            },
                KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.RepositoryPolicy
            );
        }

        public bool Exists(string name)
        {
            return _dbContext.Blogs.Any(b => b.Name == name);
        }

        public Blog Get(string name)
        {
            return _dbContext.Blogs.Where(b => b.Name == name).FirstOrDefault();
        }
        public Blog Get(int id)
        {
            return _dbContext.Blogs.Where(b => b.BlogId == id).FirstOrDefault();
        }

        public IEnumerable<Blog> GetAll()
        {
            return _dbContext.Blogs;
        }

        public bool AddNewPost(Blog blog, string title, string content)
        {
            exManager.Process(() => {
                if (blog.Posts!= null && blog.Posts.Any(p => p.Title == title))
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("Blog post with `{0}` already exists.", title));
                }
                blog.Posts.Add(new Post { Title=title, Content = content });
                if(this.SaveChanges() < 1 )
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("could create save blog post {0}", title));
                }
                return true;
            }, KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.RepositoryPolicy);
            return false;
        }

        public bool UpdatePost(Blog blog, string title, string content)
        {
            exManager.Process(() =>
            {
                Post post = blog.Posts.Where(p => p.Title == title).FirstOrDefault();
                if (post == null)
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("Blog post with `{0}` does not exists.", title));
                }
                post.Content = content;
                if (this.SaveChanges() < 1)
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("could not save blog post {0}", title));
                }
                return true;
            }, KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.RepositoryPolicy);
            return false;
        }

        public void RemovePost(Blog blog, int id)
        {
            exManager.Process(() =>
            {
                Post post = blog.Posts.Where(p => p.PostId == id).FirstOrDefault();
                if (post == null)
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("Blog post does not exists."));
                }
                _dbContext.Posts.Remove(post);
                if (this.SaveChanges() < 1)
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("could not delete blog post {0}", post.Title));
                }
            }, KitchenSinkBDD.Constants.EnvironmentConstants.ExceptionPolicies.RepositoryPolicy);
        }

        public Post GetPost(int id)
        {
            return _dbContext.Blogs.SelectMany(b => b.Posts).Where(p => p.PostId == id).FirstOrDefault();
        }

        private bool CreateBlogProcess(string name)
        {
            if (!string.IsNullOrEmpty(name) && !this.Exists(name))
            {
                Blog blog = new Blog();
                blog.Name = name;
                _dbContext.Blogs.Add(blog);
                if(this.SaveChanges() < 1)
                {
                    throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("could create blog with name {0}", name));
                }
                return true;
            }
            return false;
        }

        private void DeleteBlogProcess(int id)
        {
            _dbContext.Blogs.Remove(_dbContext.Blogs.Where(b => b.BlogId == id).FirstOrDefault());
            this.SaveChanges();
        }

        private bool UpdateBlogProcess(int id, string name)
        {
            Blog blog = _dbContext.Blogs.Where(b => b.BlogId == id).FirstOrDefault();
            blog.Name = name;
            if(_dbContext.SaveChanges() < 1)
            {
                throw new KitchenSinkBDD.Exceptions.Blog.BlogError(string.Format("unable to save changes to blog {0}", name));
            }
            return false;
        }


    }
}
