﻿using KitchenSinkBDD.Models.Blogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Repositories.Blogs
{
    public interface IBlogRepository
    {
        bool Create(string name);
        bool Update(int id, string name);
        void Delete(int id);
        bool Exists(string name);
        Blog Get(string name);
        Blog Get(int id);
        IEnumerable<Blog> GetAll();

        bool AddNewPost(Blog blog, string title, string content);
        bool UpdatePost(Blog blog, string title, string content);
        Post GetPost(int id);
        void RemovePost(Blog blog, int id);
    }
}
