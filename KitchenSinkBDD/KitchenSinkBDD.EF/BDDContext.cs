﻿using KitchenSinkBDD.Models.Blogs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.EF
{
    public class BDDContext : DbContext
    {
        public BDDContext() : base("name=BDDConnection") 
        { 
            Database.SetInitializer<BDDContext>(new CreateDatabaseIfNotExists<BDDContext>());
        }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
        }
    }
}
