﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KitchenSinkBDD.Controllers
{
    public class BlogController : Controller
    {
        private KitchenSinkBDD.Services.BloggingServices.Interfaces.IBlogService _blogService;
        public BlogController(KitchenSinkBDD.Services.BloggingServices.Interfaces.IBlogService blogService)
        {
            _blogService = blogService;
        }
        // GET: Blog
        public ActionResult Index()
        {
            return View();
        }

    }
}