﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace KitchenSinkBDD.Controllers.Api
{
    [RoutePrefix("api/blogapi")]
    public class BlogApiController : ApiController
    {
        private KitchenSinkBDD.Services.BloggingServices.Interfaces.IBlogService _service;
        private KitchenSinkBDD.DataFactory.BlogData.IBlogDataFactory _factory;

        //public BlogApiController() { }
        public BlogApiController(KitchenSinkBDD.Services.BloggingServices.Interfaces.IBlogService service, KitchenSinkBDD.DataFactory.BlogData.IBlogDataFactory factory)
        {
            _service = service;
            _factory = factory;
        }

        [System.Web.Http.HttpGet]
        [Route("blogs")]
        public HttpResponseMessage GetBlogs(HttpRequestMessage request)
        {
            IEnumerable<KitchenSinkBDD.Models.Blogs.Blog> blogs = _factory.GetAll();
            HttpResponseMessage msg = request.CreateResponse<KitchenSinkBDD.Models.Blogs.Blog[]>(System.Net.HttpStatusCode.OK, blogs.ToArray());
            return msg;
        }

        [System.Web.Http.HttpGet]
        [Route("{blogId}")]
        public HttpResponseMessage GetBlog(HttpRequestMessage request, int blogId)
        {
            return request.CreateResponse<KitchenSinkBDD.Models.Blogs.Blog>(System.Net.HttpStatusCode.OK, _factory.Get(blogId));
        }

        [System.Web.Http.HttpPost]
        [Route("create")]
        public HttpResponseMessage CreateBlog(HttpRequestMessage request, [FromBody] KitchenSinkBDD.Models.Blogs.Blog blog)
        {
            try
            {
                _service.CreateBlog(blog.Name);
            }
            catch (KitchenSinkBDD.Exceptions.Blog.BlogError error)
            {
                var msg = new {message = error.ErrorDescription} ;
                return request.CreateResponse(System.Net.HttpStatusCode.NotAcceptable, msg);
            }
            var mmsg = new { message = "successfully created new blog" };
            return request.CreateResponse(System.Net.HttpStatusCode.OK, mmsg);
        }



        [System.Web.Http.HttpGet]
        [Route("posts/{blogId}")]
        public HttpResponseMessage GetPosts(HttpRequestMessage request, int blogId)
        {
            KitchenSinkBDD.Models.Blogs.Blog blog = _factory.Get(blogId);
            HttpResponseMessage msg = request.CreateResponse<KitchenSinkBDD.Models.Blogs.Post[]>(System.Net.HttpStatusCode.OK, blog.Posts.ToArray());
            return msg;
        }

        [System.Web.Http.HttpGet]
        [Route("post/{postId}")]
        public HttpResponseMessage GetPost(HttpRequestMessage request, int postId)
        {
            return request.CreateResponse<KitchenSinkBDD.Models.Blogs.Post>(System.Net.HttpStatusCode.OK, _factory.GetPost(postId));
        }

        



    }
}