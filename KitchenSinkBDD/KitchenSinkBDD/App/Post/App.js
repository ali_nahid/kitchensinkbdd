﻿var postModule = angular.module('post', ['common'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/posts/:blogId', { templateUrl: '/App/Post/Views/PostListView.html', controller: 'postListViewModel' });
        $routeProvider.when('/posts/show/:postId', { templateUrl: '/App/Post/Views/PostView.html', controller: 'postViewModel' });
        $routeProvider.otherwise({ redirectTo: '/posts/:blogId' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });

postModule.factory('postService', function ($rootScope, $http, $q, $location, viewModelHelper) { return MyApp.postService($rootScope, $http, $q, $location, viewModelHelper); });

(function (myApp) {
    var postService = function ($rootScope, $http, $q, $location, viewModelHelper) {
        var self = this;
        self.postId = 0;
        self.blogId = 0;
        viewModelHelper.applicationpath = "";
        return this;
    };
    myApp.postService = postService;
}(window.MyApp));