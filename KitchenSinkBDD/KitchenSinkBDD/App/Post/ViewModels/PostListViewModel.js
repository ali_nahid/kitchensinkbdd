﻿postModule.controller("postListViewModel", function ($scope, postService, $http, $q, $routeParams, $window, $location, viewModelHelper) {

    $scope.viewModelHelper = viewModelHelper;
    $scope.postService = postService;
    $scope.flags = { shownFromList: false };
    var initialize = function () {
        $scope.pageHeading = "Post Section";
        $scope.postService.blogId = $routeParams.blogId;
        viewModelHelper.apiGet('api/blogapi/' + $routeParams.blogId, null,
            function (result) {
                $scope.postService.blogName = result.data.Name;
            });
        $scope.refreshPosts($routeParams.blogId);
    }

    $scope.refreshPosts = function (blogId) {
        viewModelHelper.apiGet('api/blogapi/posts/' + blogId, null,
            function (result) {
                $scope.posts = result.data;
            });
    }

    $scope.showPost = function (post) {
        $scope.flags.shownFromList = true;
        viewModelHelper.navigateTo('posts/show/' + post.PostId);
    }

    initialize();
});
