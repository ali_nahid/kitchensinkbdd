﻿postModule.controller("postViewModel", function ($scope, postService, $http, $q, $routeParams, $window, $location, viewModelHelper) {

    $scope.viewModelHelper = viewModelHelper;
    $scope.postService = postService;

    var initialize = function () {
        $scope.refreshPost($routeParams.postId);
    }

    $scope.refreshPost = function (postId) {
        viewModelHelper.apiGet('api/blogapi/post/'+ postId, null,
            function (result) {
                postService.postId = postId;
                $scope.post = result.data;
            });
    }
    
    initialize();
});
