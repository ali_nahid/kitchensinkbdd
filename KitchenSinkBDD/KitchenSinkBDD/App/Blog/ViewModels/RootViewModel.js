﻿blogModule.controller("rootViewModel", function ($scope, blogService, $http, $q, $routeParams, $window, $location, viewModelHelper) {

    $scope.viewModelHelper = viewModelHelper;
    $scope.blogService = blogService;

    $scope.flags = { shownFromList: false };

    $scope.blogName = "";

    blogService.createBlogMessage = "";

    var initialize = function () {
        $scope.pageHeading = "Blog Section";
    }

    $scope.blogList = function () {
        blogService.createBlogMessage = "";
        viewModelHelper.navigateTo('blog/list');
    }

    $scope.showBlog = function () {
        if (blogService.blogId != 0) {
            $scope.flags.shownFromList = false;
            viewModelHelper.navigateTo('blog/show/' + blogService.blogId);
        }
    }

    $scope.createBlog = function () {
        if ($scope.blogName != '') {
            viewModelHelper.apiPost('api/blogapi/create', JSON.stringify({ Name : $scope.blogName }),
                function (result) {
                    blogService.createBlogMessage = result.data.message;
                    $scope.blogName = "";
                },
                function (result) {
                    blogService.createBlogMessage = result.data.message;
                }
            );
        }
    }

    initialize();
});