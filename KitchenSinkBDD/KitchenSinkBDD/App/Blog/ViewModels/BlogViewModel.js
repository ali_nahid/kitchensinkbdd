﻿blogModule.controller("blogViewModel", function ($scope, blogService, $http, $q, $routeParams, $window, $location, viewModelHelper) {

    $scope.viewModelHelper = viewModelHelper;
    $scope.blogService = blogService;

    var initialize = function () {
        $scope.refreshBlog($routeParams.blogId);
    }

    $scope.refreshBlog = function (blogId) {
        blogService.createBlogMessage = "";
        viewModelHelper.apiGet('api/blogapi/'+blogId, null,
            function (result) {
                blogService.blogId = blogId;
                $scope.blog = result.data;
            });
    }
    
    initialize();
});
