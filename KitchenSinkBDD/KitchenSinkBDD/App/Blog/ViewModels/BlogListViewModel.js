﻿blogModule.controller("blogListViewModel", function ($scope, blogService, $http, $q, $routeParams, $window, $location, viewModelHelper) {

    $scope.viewModelHelper = viewModelHelper;
    $scope.blogService = blogService;

    var initialize = function () {
        $scope.refreshBlogs();
    }

    $scope.refreshBlogs = function () {
        blogService.createBlogMessage = "";
        viewModelHelper.apiGet('api/blogapi/blogs', null,
            function (result) {
                $scope.blogs = result.data;
            });
    }

    $scope.showBlog = function (blog) {
        $scope.flags.shownFromList = true;
        viewModelHelper.navigateTo('blog/show/' + blog.BlogId);
    }

    initialize();
});
