﻿var blogModule = angular.module('blog', ['common'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/blog', { templateUrl: '/App/Blog/Views/BlogHomeView.html', controller: 'rootViewModel' });
        $routeProvider.when('/blog/list', { templateUrl: '/App/Blog/Views/BlogListView.html', controller: 'blogListViewModel' });
        $routeProvider.when('/blog/show/:blogId', { templateUrl: '/App/Blog/Views/BlogView.html', controller: 'blogViewModel' });
        $routeProvider.otherwise({ redirectTo: '/blog' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });

blogModule.factory('blogService', function ($rootScope, $http, $q, $location, viewModelHelper) { return MyApp.blogService($rootScope, $http, $q, $location, viewModelHelper); });

(function (myApp) {
    var blogService = function ($rootScope, $http, $q, $location, viewModelHelper) {
        var self = this;
        self.blogId = 0;
        self.applicationpath = "";
        self.createBlogMessage = "";
        return this;
    };
    myApp.blogService = blogService;
}(window.MyApp));