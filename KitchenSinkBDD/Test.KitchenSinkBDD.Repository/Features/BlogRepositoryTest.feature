﻿Feature: TestBlogRepository
	In order to avoid silly mistakes
	As a blog idiot
	I want to be assured that add,update,delete to blog works
Background: Add blog Test Data
	Given I have Added the Following Blogs
	| Name         |
	| Food Blog    |
	| Pi Blog      |
	| C Sharp Blog |
@BlogRepositoryTestSteps
Scenario: Repository.Check if a blog exists
	Given I have entered "Pi Blog" to check
	Then the result should be "true"
Scenario: Repository.Add a new blog with existing name
	Given I have trying to add "Food Blog" as a new blog
	Then the result should be "false"
Scenario: Repository.Add a new blog
	Given I have trying to add "Java Blog" as a new blog
	Then the result should be "true"
