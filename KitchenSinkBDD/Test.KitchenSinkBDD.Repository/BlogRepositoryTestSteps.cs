﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using KitchenSinkBDD.Repositories.Blogs;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Moq;

using KitchenSinkBDD.Exceptions.Policies;
using KitchenSinkBDD.Exceptions;
using KitchenSinkBDD.Exceptions.Logging;
using KitchenSinkBDD.Models.Blogs;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
namespace KitchenSinkBDD.Test.Repository
{
    [Binding]
    public class BlogRepositoryTestSteps
    {
        private IBlogRepository _repository = null;
        
        public BlogRepositoryTestSteps()
        {
            IKernel kernel = new StandardKernel(new KitchenSinkBDD.Repositories.NinjectBinder());
            _repository = kernel.Get<IBlogRepository>();
        }

        [Given(@"I have Added the Following Blogs")]
        public void GivenIHaveAddedTheFollowingBlogs(Table data)
        {
            IEnumerable<Blog> blogs = data.CreateSet<Blog>();
            foreach(Blog blog in blogs)
            {
                _repository.Create(blog.Name);
            }
            //ScenarioContext.Current.Pending();
        }

        [Given(@"I have trying to add ""(.*)"" as a new blog")]
        public void GivenIHaveEnteredIntoTheBlogRepository(string blogName)
        {
            this.AddToScenario<bool>("TrueOrFalse", _repository.Create(blogName));
        }

        [Given(@"I have entered ""(.*)"" to check")]
        public void BlogCheck(string p0)
        {
            this.AddToScenario<bool>("TrueOrFalse",_repository.Exists(p0));
        }


        [Then(@"the result should be ""(.*)""")]
        public void ThenTheResultShouldBeTrueORFalse(string resultValue)
        {
            bool resultShouldBe = Convert.ToBoolean(resultValue);
            bool actualResult = ScenarioContext.Current.Get<bool>("TrueOrFalse");
            Assert.AreEqual(resultShouldBe, actualResult);
        }

        private void AddToScenario<T>(string key, T value)
        {
            if (ScenarioContext.Current.ContainsKey("TrueOrFalse"))
            {
                ScenarioContext.Current.Set<T>(value, key);
            }
            else
            {
                ScenarioContext.Current.Add(key, value);
            }
            
        }
    }
}
