﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using KitchenSinkBDD.Services.BloggingServices.Interfaces;
using KitchenSinkBDD.Services.BloggingServices;

namespace KitchenSinkBDD.Services
{
    public class NinjectBinder : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new KitchenSinkBDD.Processor.NinjectBinder());
            //Commands
            Kernel.Bind<IBlogService>().To<BlogService>().InSingletonScope(); // verified. singleton
        }
        
    }
}
