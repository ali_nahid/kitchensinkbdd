﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Services.BloggingServices.Interfaces
{
    public interface IBlogService
    {
        void DoNewPost(string blog, string title, string content);
        void CreateBlog(string blog);
        void UpdateBlog(int id,string name);
    }
}
