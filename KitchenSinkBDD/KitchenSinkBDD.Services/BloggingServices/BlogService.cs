﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KitchenSinkBDD.Processor;
using KitchenSinkBDD.Services.BloggingServices.Interfaces;
using KitchenSinkBDD.Processor.BlogProcessor.Commands;

namespace KitchenSinkBDD.Services.BloggingServices
{
    public class BlogService : IBlogService
    {
        private ICommandBus commandBus;
        public BlogService(ICommandBus _cbus)
        {
            this.commandBus = _cbus;
        }

        public void DoNewPost(string blog, string title, string content)
        {
            CreateOrUpdatePostCommand cmd = new CreateOrUpdatePostCommand(blog, title, content, Constants.CommandOperation.Create);
            this.commandBus.Send(cmd);
        }

        public void CreateBlog(string name)
        {
            CreateBlogCommand cmd = new CreateBlogCommand(name);
            this.commandBus.Send(cmd);
        }

        public void UpdateBlog(int id, string name)
        {
            UpdateBlogCommand cmd = new UpdateBlogCommand(id, name);
            this.commandBus.Send(cmd);
        }
    }
}
