﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSinkBDD.Constants
{
    public static class EnvironmentConstants
    {
        public static class ExceptionPolicies
        {
            public const string RepositoryPolicy = "RepositoryPolicy";
            public const string ProcessorPolicy = "ProcessorPolicy";
        }
    }

    public enum CommandStatus
    {
        Created,
        Handled,
        RejectedNoHandler
    }

    public enum CommandOperation
    {
        Create,
        Update,
        Delete
    }
}
